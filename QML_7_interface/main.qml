import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 300
    height: 400
    title: "Login Page"

    Rectangle {
        width: parent.width
        height: parent.height

        Column {
            anchors.centerIn: parent
            spacing: 15

            TextField {
                id: loginField
                placeholderText: "Enter your login"
                width: parent.width
                height: 30
                font.pixelSize: 16
            }

            TextField {
                id: passwordField
                placeholderText: "Enter your password"
                width: parent.width
                height: 30
                font.pixelSize: 16
                echoMode: TextInput.Password
            }

            Row {
                spacing: 10

                Button {
                    text: "Log In"
                    onClicked: {
                        console.log("Logging in with login:", loginField.text, "and password:", passwordField.text);
                    }
                }

                Button {
                    text: "Clear"
                    onClicked: {
                        loginField.text = "";
                        passwordField.text = "";
                    }
                }
            }
        }
    }
}
