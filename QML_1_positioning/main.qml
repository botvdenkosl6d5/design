import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 400
    height: 400

    TraumaLogo {
        logoColor: "red"
        logoWidth: 200
        logoHeight: 40
        logoRotation: 45
        logoText: "Trauma Team"
        logoFontSize: 25
    }
}
