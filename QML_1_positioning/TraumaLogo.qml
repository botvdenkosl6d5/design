import QtQuick 2.15


Rectangle {
    property alias logoColor: logo.color
    property alias logoWidth: logo.width
    property alias logoHeight: logo.height
    property alias logoRotation: logo.rotation
    property alias logoText: logoText.text
    property alias logoFontSize: logoText.font.pointSize

    width: parent.width
    height: parent.height

    Rectangle {
        id: logo
        width: parent.logoWidth
        height: parent.logoHeight
        color: parent.logoColor
        anchors.centerIn: parent
        rotation: parent.logoRotation
    }

    Rectangle {
        width: parent.logoWidth
        height: parent.logoHeight
        color: parent.logoColor
        anchors.centerIn: parent
        rotation: -parent.logoRotation
    }

    Rectangle {
        width: parent.logoHeight
        height: parent.logoWidth
        color: parent.logoColor
        anchors.centerIn: parent
        rotation: 0
    }

    Text {
        id: logoText
        text: parent.logoText
        font.pointSize: parent.logoFontSize
        font.family: "Arial Black"
        color: "black"
        anchors.centerIn: parent
    }
}
