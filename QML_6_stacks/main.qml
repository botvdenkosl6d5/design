import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2
ApplicationWindow {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")

    property int defMargin:10

    header:ToolBar{
        id:page_header
        height:40
        RowLayout{
            ToolButton{
                id:back_btn
                Text{
                    id:arrow
                    text: "<-"
                    font.pixelSize: 24
                    visible:stack_view.currentItem != page1
                    anchors.verticalCenter: parent.verticalCenter
                }
                onClicked: {
                    if (stack_view.currentItem == page3 || stack_view.currentItem == page2) {
                        stack_view.replace(page1)

                    }
                }

            }
            Text{
                id:header_page_text
                font.pixelSize: 24
                leftPadding: 10
                text: {
                    if (stack_view.currentItem == page1) {
                        return "It's first page"
                    }
                    if (stack_view.currentItem == page3) {
                        return "Yellow page"
                    }
                    if (stack_view.currentItem == page2) {
                        return "Red page"
                    }

                }
            }
        }
    }


    StackView{
        id: stack_view
        anchors.fill: parent
        initialItem: page1
    }

    MyPage {
        id:page1
        backgroundColor: "red"
        buttonText: "To_Green"
        buttonText2: "To_Yellow"
        onButtonClicked: {
            stack_view.replace(page2, StackView.PopTransition)
        }
        onButtonClicked2: {
            stack_view.replace(page3, StackView.PushTransition)
        }
    }
    MyPage {
        id:page2
        visible: false
        backgroundColor: "green"
        buttonText: "To_Yellow"
        buttonText2: "To_Red"
        onButtonClicked: {
            stack_view.replace(page3, StackView.PopTransition)
        }
        onButtonClicked2: {
            stack_view.replace(page1, StackView.PushTransition)
        }
    }
    MyPage {
        id:page3
        visible: false
        backgroundColor: "yellow"
        buttonText: "To_Red"
        buttonText2: "To_Green"
        onButtonClicked: {
            stack_view.replace(page1, StackView.PopTransition)
        }
        onButtonClicked2: {
            stack_view.replace(page2, StackView.PushTransition)
        }
    }
}
