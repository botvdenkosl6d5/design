import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

ToolBar {
    id: pageHeader
    height: 40

    property StackView stackView

    RowLayout {
        ToolButton {
            id: backButton
            text: "<-"
            font.pixelSize: 24
            visible: stackView.depth > 1
            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft

            onClicked: {
                stackView.pop()
            }
        }

        Text {
            id: headerPageText
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            text: "Your Page Title"
        }
    }
}
