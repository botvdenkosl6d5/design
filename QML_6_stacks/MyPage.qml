import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {
    id: root

    property alias backgroundColor: back_fon.color
    property alias buttonText: button_nav.text
    property alias buttonText2: button_nav2.text

    signal buttonClicked();
    signal buttonClicked2();

    background: Rectangle {
        id: back_fon
    }

    Button {
        id: button_nav
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            root.buttonClicked();
        }
    }

    Button {
        id: button_nav2
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            root.buttonClicked2();
        }
    }
}
