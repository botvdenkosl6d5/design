import QtQuick 2.0
import QtQuick.Layouts

Window {
     title: qsTr("Column Layout")
    visible: true
    width: 800
    height:200

    RowLayout {
        anchors.fill: parent

        ColorComp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        ColorComp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        ColorComp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
