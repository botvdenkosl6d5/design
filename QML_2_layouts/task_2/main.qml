import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 480; height: 850; visible: true
    title: qsTr("Dark theme")
    color: "#171717"
    id: win

    Item {
        id: root
        anchors.fill: parent

        CustomRectangle {
            id: rect_top
            compText: "Header"
            compColor: "#303030"
            compBorderColor: "#6b6b6b"
            compHeight: parent.height * 0.1
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10
        }

        CustomRectangle {
            id: rect_mid
            compText: "Content"
            compColor: "#404040"
            compBorderColor: "#6b6b6b"
            compHeight: parent.height * 0.1
            anchors.top: rect_top.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: rect_bot.top
            anchors.margins: 20
        }

        CustomRectangle {
            id: rect_bot
            compText: ""
            compHeight: parent.height * 0.1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 10

            Row {
                anchors.fill: parent

                CustomRectangle {
                    compText: "1"
                    compColor: "#656565"
                    compBorderColor: "#6b6b6b"
                    compWidth: parent.width / 3
                    anchors.left: parent.left
                    compHeight: parent.height
                }

                CustomRectangle {
                    compText: "2"
                    compColor: "#656565"
                    compBorderColor: "#6b6b6b"
                    compWidth: parent.width / 3
                    anchors.horizontalCenter: parent.horizontalCenter
                    compHeight: parent.height
                }

                CustomRectangle {
                    compText: "3"
                    compColor: "#656565"
                    compBorderColor: "#6b6b6b"
                    compWidth: parent.width / 3
                    anchors.right: parent.right
                    compHeight: parent.height
                }


            }

        }
    }
}
