import QtQuick 2.15

Rectangle {
    id: rectComp
    property alias compWidth: rectComp.width
    property alias compHeight: rectComp.height
    property alias compColor: rectComp.color
    property alias compBorderColor: rectComp.border.color
    property alias compText: customText.text
    property alias textColor: customText.color

    width: parent.width
    height: parent.height * 0.1
    color: "lightgray"
    border.color: "lightgray"

    Text {
        id: customText
        text: ""
        color: "white"
        font {
            family: "Futura"
            pointSize: 16
        }
        anchors.centerIn: parent
    }
}

