// PageFooter.qml
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {
    id: root
    width: parent.width
    height: 60
    anchors {
        bottom: parent.bottom
    }

    gradient: Gradient {
        GradientStop { position: 0; color: "lightgray" }
        GradientStop { position: 1; color: "white" }
    }

    signal newMessage(string msg, string time)

    TextField {
        id: edtText
        anchors.fill: parent
        placeholderText: "Write a message..."
        font.pointSize: 10
        color: "black"
    }

    Button {
        id: btnAddItem
        height: root.height
        anchors.right: root.right
        text: "Send"
        onClicked: {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var timeString = hours + ":" + (minutes < 10 ? "0" : "") + minutes;

            newMessage(edtText.text, timeString);
            edtText.clear();
        }
    }
}
