// main.qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Smessenger")

    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0; color: "#ff7f50" }
            GradientStop { position: 1; color: "#ffd700" }
        }

        Layout.fillWidth: true
        Layout.fillHeight: true

        Rectangle {
            id: messagesContainer
            width: parent.width
            height: parent.height
            anchors.centerIn: parent
            color: "transparent"

            ListModel {
                id: my_model
                ListElement { message: "Hello"; sendTime: "12:30" }
                ListElement { message: "Are you here?"; sendTime: "12:35" }
                ListElement { message: "I need help"; sendTime: "12:35" }
            }

            Component {
                id: my_delegate
                MyDelegate {
                    message: model.message
                    sendTime: model.sendTime
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width - 150
                    height: 40
                }
            }

            ListView {
                id: my_list
                anchors.fill: parent
                model: my_model
                delegate: my_delegate

                spacing: 8
            }


            PageFooter {
                width: messagesContainer.width
                anchors {
                    bottom: messagesContainer.bottom
                }
                onNewMessage: {
                    my_model.append({ message: msg, sendTime: time })
                }
            }
        }
    }
}
