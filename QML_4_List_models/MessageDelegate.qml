import QtQuick

Item {
    id: messageDelegateItem

    property string text: "?"
    property string timestamp: "00:00"

    Rectangle {
        id: rect
        border.color: "darkgrey"
        radius: 5
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0; color: "lightgray" }
            GradientStop { position: 1; color: "white" }
        }
    }

    Row {
        id: row
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height
        anchors.margins: 16
        spacing: 6

        Text {
            id: timestampText
            text: timestamp
            anchors.verticalCenter: row.verticalCenter
            font.pixelSize: 12
        }

        Text {
            text: text
            anchors.verticalCenter: row.verticalCenter
            wrapMode: Text.WordWrap
            width: parent.width - timestampText.width - 20
        }
    }

}
