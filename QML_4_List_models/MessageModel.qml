// MessageModel.qml
import QtQml.Models 2.15

ListModel {
    id: messageModel

    ListElement { text: "Привет! Как дела?"; time: "10:00" }
    ListElement { text: "Привет! Всё отлично, спасибо!"; time: "10:05" }
}
