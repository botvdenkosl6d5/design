// MyDelegate.qml
import QtQuick
import Felgo

Item {
    id: deleg
    property string message: "?"
    property string sendTime: "?"

    Rectangle {
        id: rect
        border.color: "darkgrey"
        radius: 5
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0; color: "lightgray" }
            GradientStop { position: 1; color: "white" }
        }
    }

    Row {
        id: row
        anchors.left: parent.left
        anchors.right: parent.right
        height: Math.max(textEdit.height, parent.height)
        anchors.margins: 8
        spacing: 22


        TextEdit {
            id: textEdit
            text: message
            readOnly: true
            wrapMode: TextEdit.Wrap
            width: parent.width - 30
            height: parent.height


            verticalAlignment: Text.AlignVCenter
        }


        Text {
            text: sendTime
            anchors.verticalCenter: row.verticalCenter
            anchors.right: row.right
            anchors.rightMargin: 8
            anchors.bottom: row.bottom
            anchors.bottomMargin: 24
        }
    }
}
